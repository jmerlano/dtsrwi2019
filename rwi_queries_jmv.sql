
-- explore which deals have rwi and no escrow:
select dt.deal_id, deal_name
from data.dealterms dt
join data.buyer_rwi b on (dt.deal_id=b.deal_id and b.buyer_rwi='Y')
-- where dt.indm_escrow_dollar_amount_escrow_holdback is null ;
where escrow_size is null ;

-- explore which deals have rwi and no escrow:
select dt.deal_id, deal_name
from data.dealterms dt
join data.buyer_rwi b on (dt.deal_id=b.deal_id and b.buyer_rwi='Y')
join data.line_of_business ln on (ln.deal_id=dt.deal_id and ln.shareholder_rep='Yes')
-- where indm_escrow_dollar_amount_escrow_holdback is null ;
where escrow_size is null ;


-- Number of RWI deals per year

SELECT min(extract('year' FROM ic_deal_close_date)) AS yr, count(dt.deal_id) AS rwi_count,
       (SELECT count(deal_id)
        FROM data.dealterms
	WHERE extract('year' FROM ic_deal_close_date) = min(extract('year' FROM dt.ic_deal_close_date))
	) AS year_count
FROM data.dealterms dt
JOIN data.buyer_rwi b ON (dt.deal_id = b.deal_id AND b.buyer_rwi = 'Y')
-- where extract('year' FROM ic_deal_close_date) = 2018
GROUP BY extract('year' FROM ic_deal_close_date)
ORDER BY extract('year' FROM ic_deal_close_date) ;


SELECT min(extract('year' FROM ic_deal_close_date)) AS yr, count(dt.deal_id) AS rwi_count,
       (SELECT count(deal_id)
        FROM data.dealterms
	WHERE extract('year' FROM ic_deal_close_date) = min(extract('year' FROM dt.ic_deal_close_date))
	) AS year_count
FROM data.dealterms dt
-- JOIN data.buyer_rwi b ON (dt.deal_id = b.deal_id AND b.buyer_rwi = 'Y')
JOIN data.customer c ON (dt.deal_id = c.deal_id AND c.buy_side_rwi)
-- where extract('year' FROM ic_deal_close_date) = 2018
GROUP BY extract('year' FROM ic_deal_close_date)
ORDER BY extract('year' FROM ic_deal_close_date) ;


-- Number of RWI deals per year + percentages

WITH deal_counts AS (
   SELECT extract('year' FROM ic_deal_close_date) AS yr, count(deal_id) AS deal_count
   FROM data.dealterms
   GROUP BY extract('year' FROM ic_deal_close_date)
),
numbers AS (
   SELECT min(extract('year' FROM ic_deal_close_date)) AS yr, count(dt.deal_id) AS rwi_count,
	  (SELECT deal_count
	   FROM deal_counts dc
	   WHERE dc.yr = min(extract('year' FROM ic_deal_close_date))) AS year_count
   FROM data.dealterms dt
   JOIN data.buyer_rwi b ON (dt.deal_id = b.deal_id AND b.buyer_rwi = 'Y')
   GROUP BY extract('year' FROM ic_deal_close_date)
   ORDER BY extract('year' FROM ic_deal_close_date)
)
SELECT *, round(100 * rwi_count::numeric / year_count, 2) AS pct
FROM numbers ;


-- rwi_transaction_size.R

SELECT deal_size, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE deal_size IS NOT NULL
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_bpr.R

SELECT etc_buyer_market_cap / deal_size AS bpr, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep = 'Yes')
WHERE deal_size IS NOT NULL
   AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_escrow_compare.R

SELECT to_char(round(avg(coalesce(ic_escrow_percent_of_purchase_price, 0))::numeric, 1), '999.9') AS avg_esc_non_rwi,
       to_char(round(median(ic_escrow_percent_of_purchase_price)::numeric, 1), '999.9') AS median_esc_non_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id=dt.deal_id AND ln.shareholder_rep='Yes')
WHERE ic_escrow_percent_of_purchase_price IS NOT NULL
  AND coalesce(r.buyer_rwi, 'N') = 'N'
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


SELECT to_char(round(avg(coalesce(ic_escrow_percent_of_purchase_price, 0))::numeric, 1), '999.9') AS avg_esc_non_rwi,
       to_char(round(median(ic_escrow_percent_of_purchase_price)::numeric, 1), '999.9') AS median_esc_non_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id=dt.deal_id AND ln.shareholder_rep='Yes')
WHERE ic_escrow_percent_of_purchase_price IS NOT NULL
  AND coalesce(r.buyer_rwi, 'N') = 'Y'
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_escrow_size.R

SELECT coalesce(ic_escrow_percent_of_purchase_price, 0) AS ic_escrow_percent_of_purchase_price,
       coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE ic_escrow_percent_of_purchase_price IS NOT NULL
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


SELECT min(coalesce(ic_escrow_percent_of_purchase_price, 0)::numeric) AS min_percent,
       max(coalesce(ic_escrow_percent_of_purchase_price, 0)::numeric) AS max_percent
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE ic_escrow_percent_of_purchase_price IS NOT NULL
  -- AND ic_escrow_percent_of_purchase_price = 0
  AND coalesce(r.buyer_rwi, 'N') = 'Y'
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;

 min_percent | max_percent 
-------------+-------------
      0.2266 |        37.5
-- Yes. Range is min percent 0.22% and max percent 37.5%
    

-- rwi_fin_ppa_included.R

SELECT fin_ppa_included, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE indm_materiality_scrape IS NOT NULL
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_fin_ppa_separate_escrow.R

SELECT fin_ppa_separate_escrow, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE indm_materiality_scrape IS NOT NULL
AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_10b5_fd_rep.R

SELECT rw_10b5_full_disclosure_representations_included, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE rw_10b5_full_disclosure_representations_included IS NOT NULL
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_non_reliance.R

SELECT rw_no_other_reps_or_non_reliance_included, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt
LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE rw_no_other_reps_or_non_reliance_included IS NOT NULL
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;


-- rwi_pq_fwd_looking_details.R

SELECT pq_mae_fwd_looking_details, coalesce(buyer_rwi, 'N') AS buyer_rwi
FROM data.dealterms dt LEFT JOIN data.buyer_rwi r ON dt.deal_id = r.deal_id
JOIN data.line_of_business ln ON (ln.deal_id = dt.deal_id AND ln.shareholder_rep='Yes')
WHERE pq_mae_fwd_looking_details IS NOT NULL
  AND extract('year' FROM ic_deal_close_date) BETWEEN 2016 AND 2018 ;



