
-- explore which deals have rwi and no escrow:
select dt.deal_id, deal_name from data.dealterms dt join data.buyer_rwi b on (dt.deal_id=b.deal_id and b.buyer_rwi='Y')
where indm_escrow_dollar_amount_escrow_holdback is null;

-- explore which deals have rwi and no escrow:
select dt.deal_id, deal_name from data.dealterms dt
join data.buyer_rwi b on (dt.deal_id=b.deal_id and b.buyer_rwi='Y')
join data.line_of_business ln on (ln.deal_id=dt.deal_id and ln.shareholder_rep='Yes')
where indm_escrow_dollar_amount_escrow_holdback is null;



