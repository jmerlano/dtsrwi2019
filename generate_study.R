################################################################################
### Confidential and trade secret information of SRS Acquiom, Inc.           ###
### In case of disclosure, contents are:                                     ###
### Copyright (c) 2018 SRS Acquiom, Inc. All Rights Reserved.                ###
################################################################################
###
### Description: Automated generation for Deal Terms Study
### Author: Glenn A. Kramer


require(ReporteRs)
require(stringr)
require(XLConnect)

### IMPORTANT: FOR GRAPHICS HACKS IN graphics.R
DTS.rendering <- TRUE

source("../dtsutils2019reporters/connect.R")
source("../dtsutils2019reporters/graphics.R")
source("../dtsutils2019reporters/charts.R")
source("../dtsutils2019reporters/preprocessor.R")
source("markdown.R")
source("../dtsutils2019reporters/chart_types/pie/pie.R")

### flexibility in setting legends:
### for most elements

# (jm) Updated binary-category labels for RWI presence / absence
# RWI.LEGEND <- c("No / Unknown RWI", "RWI Included");

# RWI binary category (label 1): "No RWI Identified"
# RWI binary category (label 2): "RWI Identified"
RWI.LEGEND <- c("No RWI Identified", "RWI Identified");


DTS.rendering <- FALSE

DTSUTILS <- "../dtsutils2019reporters/"


### constants
P <- list()

P$titlebar_h <- 0.35
P$footnote_bottom <- 6.5
P$col1 <- 0.42
P$col2 <- 5.12
P$full_w <- 9.17
P$half_w <- 4.47
### slightly confusing -- these titles are for slide title, not the titles of charts; those have T_titlebar_y and T_titlebar_h, B_titlebar_y, etc.
P$title_x <- 0.34
P$title_y <- 0.27
P$title_h <- 0.7
P$preamble_y <- 1.18
P$preamble_line_h <- 0.4
P$footnote_line_h <- 0.2
P$T_titlebar_y <-  1.23
P$T_chart_y <- P$T_titlebar_y + P$titlebar_h + 0.22
P$T_chart_h <- P$footnote_y - P$T_chart_y
### guesstimates
P$B_titlebar_y <- 4.17 # 4.17
P$B_chart_y <- P$B_titlebar_y + P$titlebar_h + 0.22
P$B_chart_h <- P$footnote_y - P$B_chart_y + 0.3 # add 0.1
### factoid will go above the footnote (if there is one) ydiff is the amount above the footnote, h is the height
P$factoid_ydiff <- 0
P$factoid_h <- 0
P$factoid_y <- 0
### minimum footnote height
P$footnote_min_h <- 0.3


### setup rest of page constants based on preamble and footnote lines
page_setup <- function (preamble_lines=1, footnote_lines=1, factoid_lines=0) {

    P$titlebar_h <<- 0.35
    P$footnote_bottom <<- 6.5
    P$col1 <<- 0.44 # 0.42
    P$col2 <<- 5.14 # 5.12
    P$full_w <<- 9.17
    P$half_w <<- 4.47
    ## slightly confusing -- these titles are for slide title, not the titles of charts; those have T_titlebar_y and T_titlebar_h, B_titlebar_y, etc.
    P$title_x <<- 0.34
    P$title_y <<- 0.27
    P$title_h <<- 0.7
    P$preamble_y <<- 1.18
    P$preamble_line_h <<- 0.4
    P$footnote_line_h <<- 0.2
    P$T_titlebar_y <<-  1.23
    P$T_chart_y <<- P$T_titlebar_y + P$titlebar_h + 0.22
    P$T_chart_h <<- P$footnote_y - P$T_chart_y + 0.3 # added 0.3
    ## guesstimates
    P$B_titlebar_y <<- 4.17
    P$B_chart_y <<- P$B_titlebar_y + P$titlebar_h + 0.22
    P$B_chart_h <<- P$footnote_y - P$B_chart_y + 0.3 # added 0.3

    
    
    P$preamble_lines <<- preamble_lines # meant to be changed
    P$footnote_lines <<- footnote_lines # meant to be changed

    P$T_titlebar_y <<-  1.23 + P$preamble_lines * P$preamble_line_h 
    P$T_chart_y <<- P$T_titlebar_y + P$titlebar_h + 0.22
    P$footnote_y <<- P$footnote_bottom - P$footnote_lines * P$footnote_line_h
    ## factoid will go above the footnote (if there is one) ydiff is the amount above the footnote, h is the height
    if (factoid_lines>0) {
        P$factoid_ydiff <<- 0.6
        P$factoid_h <<- 0.5
    }
    P$T_chart_h <<- (P$footnote_y - P$factoid_ydiff) - P$T_chart_y 
    P$B_titlebar_y <<- 4.17
    P$B_chart_y <<- P$B_titlebar_y + P$titlebar_h + 0.22
    P$B_chart_h <<- P$footnote_y - P$B_chart_y + 0.1 # added 0.1 (use some footnote space)

}

page_setup()

# template <- "templates/rwi2019_template.pptx"

template <- "templates/rwi2019_template_bugfix.pptx"


### reset useful par parameters
## par_reset <- function () {
##     par(adj = 0.5, ann = TRUE, 
##         bty = "o", cex = 1, cex.axis = 1, 
##         cex.lab = 1, cex.main = 1.2, cex.sub = 1, 
##         crt = 0, err = 0, 
##         fig = c(0, 1, 0, 1), 
##         font.axis = 1, font.lab = 1, font.main = 2, font.sub = 1, 
##         lab = c(5, 5, 7), las = 0, lend = "round", lheight = 1, 
##         ljoin = "round", lmitre = 10, lty = "solid", lwd = 1, 
##         mai = c(1.02, 0.82, 0.82, 0.42),
##         ## mar = c(5.1, 4.1, 4.1, 2.1),
##         mar = c(0.1, 0.1, 0.1, 0.1),
##         mex = 1, mfcol = c(1, 1), mfg = c(1, 1, 1, 1), 
##         ##mfrow = c(1, 1),
##         mgp = c(3, 1, 0), mkh = 0.001, 
##         oma = c(0, 0, 0, 0), omd = c(0, 1, 0, 1), omi = c(0, 0, 0, 0),
##         pch = 1, ps = 12, pty = "m", 
##         smo = 1, srt = 0, tck = NA, tcl = -0.5, usr = c(0, 1, 0, 1),
##         xaxp = c(0, 1, 5), xaxs = "r", xaxt = "s", 
##         xpd = FALSE, yaxp = c(0, 1, 5), yaxs = "r", yaxt = "s",
##         family = "Georgia",
##         ylbias = 0.2)
## }



### blank function for creating headings, etc.
### blank <- function () {par(mar=c(0.001, 0.001, 0.001, 0.001)); plot.new()}
blank <- function () {par(mar=c(0, 0, 0, 0)); plot.new()}

HEADING.TP <- textProperties(font.size=13, font.family="Georgia", font.weight="normal", font.style="normal", color="white")
BOLDHEADING.TP <- textProperties(font.size=13, font.family="Georgia", font.weight="bold", font.style="normal", color="white") 
HEADING.PP <- parProperties(text.align="left", padding=0)

DECOY.TP <- textProperties(font.size=13, font.family="Georgia", font.weight="normal", font.style="normal", color=SRSCOLORS$titlebar)


PREAMBLE.TP <- textProperties(font.size=15, font.family="Georgia", font.weight="normal", font.style="normal", color="#5e6061")
FOOTNOTE.TP <- textProperties(font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c")

sacrifice <- function () {
    ### sacrifical POS POT
    DOC <<- addParagraph(DOC, pot("sacrifice", BOLDHEADING.TP), offx=19.0, offy=0.75, width=0.5, height=0.39, par.properties=parProperties(padding=0))
    DOC <<- addParagraph(DOC, pot("sacrifice", HEADING.TP), offx=29.0, offy=1.25, width=0.5, height=0.39, par.properties=parProperties(padding=0))
}




### closure to use as factory for readers of the spec chunked into portions we want to process (pages).
spec_reader <- function () {
    ns_mark <- 0  # state variable in this closure
    ## return the next slide or NULL if no more
    function () {
        ## see if there is another new_slide to be had
        if (ns_mark > length(new_slide_idx)) {return(NULL)}
        ns_prior_mark <- ns_mark
        ns_mark <<- ns_mark +1
        i1 = 1
        if (ns_prior_mark != 0) {i1 = new_slide_idx[ns_prior_mark]}
        i2 = new_slide_idx[ns_mark] - 1
        if (is.na(i2)) {i2 <- nrow(spec)}
        df <- spec[i1:i2, ]
        df
    }
}

### now we can spawn a reader that will read all the sections in sequequence, until it hits the end, returning null
### apply as df <- read_next()


### now to consider cases
has_new_slide <- function (df) {
    "new_slide" %in% na.omit(df$colA)
}

### add any global variables we find 
process_globals <- function (df) {
    mapply(function (colA, name, val) {if (!is.na(colA) && colA == "global") {GLOBALS_add(name, val)}}, df$colA, df$name, df$type)
}

### canonicalize the types of pages we know about; will will dispatch based on these types. Types we handle are:
### "TL TR"
### "T"
### "T B"
get_content_signature <- function (df) {
    if ( length(grep("(TR|TL|T|BR|BL|B)_(.*)", na.omit(df$name))) == 0 ) {
        "blank"
    } else {
        do.call(paste, as.list(sort(unique(mapply(function(x) {sub("_.*", "", x)}, grep("(TR|TL|T|BR|BL|B)_(.*)", na.omit(df$name), value=TRUE))))))
    }
}

get_comment <- function (df) {
    df [ grepl("^comment$", df$name), ]$value[1]
}


get_footnote_nudge <- function (df) {
    df [ grepl("^footnote$", df$name), ]$colB[1]
}

get_factoid_nudge <- function (df) {
    df [ grepl("^factoid$", df$name), ]$colB[1]
}

get_plot_nudge <- function (df, plotid) {
    df [ grepl("^plot$", df$type) & df$value==plotid, ]$colB[1]
}
    

guess_preamble_lines <- function (txt) {if (!is.na(txt) && txt != "") {1} else {0}}

guess_footnote_lines <- function (txt) {if (!is.na(txt) && txt != "") {1} else {0}}

guess_factoid_lines <- function (txt) {if (!is.na(txt) && txt != "") {1} else {0}}


### a bunch of helper functions for our slide dispatch functions.
start_slide <- function (df) {
    layout <- df [ grepl("^layout$", df$name), ]$value[1]
    DOC <<- addSlide(DOC, layout)
}

do_title <- function (df) {
    ## get slide title text if present, and add to slide
    title_text <- df [ grepl("^title$", df$name), ]$value[1]
    if (!is.na(title_text) & title_text != "") {
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(title_text), font.family="Arial", font.weight="normal", font.style="normal", font.size=16, color=SRSCOLORS$richblack),
                             offx=P$title_x, offy=P$title_y, width=P$full_w, height=P$title_h, par.properties=parProperties(padding=0))
    }
}

do_divtitle <- function (df) {
    ## get slide divider title text if present, and add to slide
    title_text <- df [ grepl("^title$", df$name), ]$value[1]
    title_y <- df [ grepl("^title$", df$name), ]$colA[1]
    cat("title_y =", title_y, "\n")
    y <- 2.16
    if (is.na(title_y) || is.null(title_y)) {
    } else {
        y <- as.numeric(title_y)
        cat("Altered y\n")
    }
    if (!is.na(title_text) & title_text != "") {
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(title_text), font.family="Georgia", font.weight="normal", font.style="normal", font.size=51, color=SRSCOLORS$richblack),
                             offx=2.04, offy=y, width=6.1, height=4, par.properties=parProperties(padding=0))
    }
}


get_preamble <- function (df) {df [ grepl("^preamble$", df$name), ]$value[1]}

get_footnote <- function (df) {df [ grepl("^footnote$", df$name), ]$value[1]}

get_factoid <- function (df) {df [ grepl("^factoid$", df$name), ]$value[1]}


do_preamble <- function (df, preamble_text) {
    if (!is.na(preamble_text)) {
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(preamble_text), font.family="Georgia", font.weight="normal", font.style="normal", font.size=15, color="#5e6061"),
                            offx=P$col1, offy=P$preamble_y, width=P$full_w, height=0.4, par.properties=parProperties(padding=0))
    }
}


### content.type = c("TL", "TR", "BL", "BR", "T", "B")
### a block may need to know it has a bottom neighbor; use has.lower to pass that info
do_block_layout <- function (df, content.type="T", has.lower=FALSE) {
    title_tag <- paste0("^", content.type, "_title$")
    content_tag <- paste0("^", content.type, "_content$")
    xpos <- P$col1
    if (content.type %in% c("TR", "BR")) {
        xpos <- P$col2
    }
    titlebar_y <- P$T_titlebar_y
    if (content.type %in% c("BL", "BR", "B")) {
        titlebar_y <- P$B_titlebar_y
    }
    chart_ypos <- P$T_chart_y
    if (content.type %in% c("BL", "BR", "B")) {
        chart_ypos <- P$B_chart_y
    }
    chart_h <- P$T_chart_h
    if (content.type %in% c("BL", "BR", "B")) {
        chart_h <- (chart_h / 2) - 0.1 # GAK: make same height has top chart
        ## chart_h <- P$B_chart_h
    }
    ## if has.lower is true, AND if there is no Bx_title entry, then give that space to the chart
    if (has.lower) {
        lower_titletext <- df [ grepl(paste0(gsub("T", "B", content.type), "_title"), df$name), ]$value[1]
        if (is.na(lower_titletext)) {
            cat("Found more space for chart due to no title in lower area...\n")
            chart_h <- chart_h + 0.6
        }
    } # end has.lower
    ## cut chart height in half for dual charts
    if (has.lower) {
        chart_h <- (chart_h / 2) - 0.1
    }
    width <- P$half_w
    if (content.type %in% c("T", "B")) {
        width <- P$full_w
    }
    ## finally, remove factoid space from chart height
    chart_h <- chart_h - P$factoid_h
    
    ## cat("key layout params for block", content.type, ":\n")
    ## cat("xpos = ", xpos, "; titlebar_y = ", titlebar_y, "; chart_ypos = ", chart_ypos, "; chart_h = ", chart_h, "\n")
    ## do titlebar
    layout_titletype <- df [ grepl(title_tag, df$name), ]$type[1]
    if (!is.na(layout_titletype) && layout_titletype == "blackbar") { 
        ## black bar
        DOC <<- addPlot(DOC, blank, offx=xpos, offy=titlebar_y, width=width, height=P$titlebar_h, bg=SRSCOLORS$titlebar)
        par_reset() 
        ## left title text
        layout_titletext <- df [ grepl(title_tag, df$name), ]$value[1]
        if (!is.na(layout_titletext)) {
            ## for some stupid reason, the first paragraph added in the titlebar come out bold, like it or not, so we do a "decoy" in the background color, then do the real one...
            addParagraph(DOC, pot(preprocess(layout_titletext), DECOY.TP), offx=xpos, offy=titlebar_y+0.02, width=width, height=P$titlebar_h, par.properties=parProperties(padding=0))
            ## now do the real one.
            addParagraph(DOC, pot(preprocess(layout_titletext), HEADING.TP), offx=xpos, offy=titlebar_y+0.02, width=width, height=P$titlebar_h, par.properties=parProperties(padding=0))
        }
    } else if (is.na(layout_titletype)) {
        ## we don't have a title, so need to move the content box up by titlebar_h
    } else {
        stop("layout_titletype: I don't know how to handle type: ", layout_titletype)
    }
    ## and  content
    layout_contenttype <- df [ grepl(content_tag, df$name), ]$type[1]
    if (!is.na(layout_contenttype) && layout_contenttype == "plot") {
        ## do a plot; first, get source file
        layout_contentval <- df [ grepl(content_tag, df$name), ]$value[1]
        if (is.na(layout_contentval) || substring(layout_contentval, 1, 6) != "CHARTS") {
            stop("Expecting a chart for content: ", layout_contentval)
        }
        chart_name <- sub("^.*\\[\"([^\"]*)\"\\]$", "\\1", layout_contentval)
        ## Tell the world we are rendering the study
        DTS.rendering <<- TRUE
        source_file <- paste0(DTSUTILS, list.files(pattern=paste0("^", chart_name, ".R$"), recursive=TRUE, path=DTSUTILS)[1])
        cat("Working on chart: ", chart_name, "\n")
        ## source the file and plot; be able to proceed with errors
        plot <- NULL
        py <- chart_ypos
        ##if (!is.null(get_plot_nudge(df, paste0("CHARTS[\"",chart_name,"\"]")))) {py <- py + get_plot_nudge(df, paste0("CHARTS[\"",chart_name,"\"]"))}
        try({source(source_file, chdir=TRUE); par_reset(); plot <- addPlot(DOC, CHARTS[[chart_name]], fontname_serif="Georgia", fontname_sans="Georgia", fontname_mono="Georgia", offx=xpos, offy=py, width=width, height=chart_h); par_reset();})
        if (is.null(plot)) {
            cat("&&&&&&&&&&&&&&&&&&&&&&&&&&&&& PLOT ERROR &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n")
            plot <- addPlot(DOC, blank, offx=xpos, offy=py, width=width, height=chart_h, bg="#ff0000")
            par_reset()
        }
        DOC <<- plot

        ## tell the world we are done rendering
        DTS.rendering <<- FALSE
    } else if (!is.na(layout_contenttype) && layout_contenttype == "text") {
        layout_contenttext <- df [ grepl(content_tag, df$name), ]$value[1]
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(layout_contenttext), font.size=13, font.family="Georgia", color=SRSCOLORS$richblack),
                             offx=xpos, offy=chart_ypos, width=width, height=chart_h, par.properties=parProperties(padding=0))
    } else {
        stop("layout_contenttype: I don't know how to handle type: ", layout_contenttype)
    }
    
}


T_titlebar_y_override <- function (df, types) {
    ## if TL or TR titlebar_y override is present, then set P$T_titlebar_y accordingly
    expr = paste0("^(", paste(types, collapse="|"), ")_")
    titlebar_y <- mean(as.numeric( df [ grepl(expr, df$name) & !is.na(df$colA), ]$colA ))
    if (!is.nan(titlebar_y)) {P$T_titlebar_y <<- titlebar_y}
    ## and calculate dependent variables
    P$T_chart_y <<- P$T_titlebar_y + P$titlebar_h + 0.22
    P$T_chart_h <<- P$footnote_y - P$T_chart_y
}



B_titlebar_y_override <- function (df, types) {
    ## if BL or BR titlebar_y override is present, then set P$B_titlebar_y accordingly
    expr = paste0("^(", paste(types, collapse="|"), ")_")
    titlebar_y <- mean(as.numeric( df [ grepl(expr, df$name) & !is.na(df$colA), ]$colA ))
    if (!is.nan(titlebar_y)) {P$B_titlebar_y <<- titlebar_y}
    ## and calculate dependent variables
    P$B_chart_y <<- P$B_titlebar_y + P$titlebar_h + 0.22
    P$B_chart_h <<- P$footnote_y - P$B_chart_y
}



do_divider <- function (df) {
    start_slide(df)
    do_divtitle(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    ## footnote
    fn_text <- get_footnote(df)
    if (!is.na(fn_text)) {
        ## style is different on this page: darker color, and not italic
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="normal", color=SRSCOLORS$richblack),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }
}



do_TL_TR <- function (df) {
    start_slide(df)
    do_title(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    preamble_text <- get_preamble(df)
    fn_text <- get_footnote(df)
    factoid_text <- get_factoid(df)
    ## setup page parameters
    page_setup(guess_preamble_lines(preamble_text), guess_footnote_lines(fn_text), guess_factoid_lines(factoid_text))
    ## add preamble if present
    do_preamble(df, preamble_text)
    
    ## if TL or TR titlebar_y override is present, then set P$T_titlebar_y accordingly
    T_titlebar_y_override(df, c("TL", "TR"))

    do_block_layout(df, content.type="TL")
    do_block_layout(df, content.type="TR")

    ## factoid
    if (!is.na(factoid_text)) {
        P$factoid_y <<- P$footnote_y - P$factoid_ydiff;
        if (!is.na(get_factoid_nudge(df))) {P$factoid_y <<- P$factoid_y + get_factoid_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(factoid_text), font.size=14, font.family="Georgia", font.weight="bold", font.style="normal", color="#ed8b00"),
                            offx=P$col1, offy=P$factoid_y, width=P$full_w, height=P$factoid_h, par.properties=parProperties(padding=0))
    }
    
    ## footnote
    if (!is.na(fn_text)) {
        if (!is.na(get_footnote_nudge(df))) {P$footnote_y <<- P$footnote_y + get_footnote_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c"),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }

}


do_T <- function (df) {
    start_slide(df)
    do_title(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    preamble_text <- get_preamble(df)
    fn_text <- get_footnote(df)
    factoid_text <- get_factoid(df)
    ## setup page parameters
    page_setup(guess_preamble_lines(preamble_text), guess_footnote_lines(fn_text), guess_factoid_lines(factoid_text))
    ## add preamble if present
    do_preamble(df, preamble_text)
    
    
    ## if T titlebar_y override is present, then set P$T_titlebar_y accordingly
    T_titlebar_y_override(df, c("T"))

    do_block_layout(df, content.type="T")

    ## factoid
    if (!is.na(factoid_text)) {
        P$factoid_y <<- P$footnote_y - P$factoid_ydiff;
        if (!is.na(get_factoid_nudge(df))) {P$factoid_y <<- P$factoid_y + get_factoid_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(factoid_text), font.size=14, font.family="Georgia", font.weight="bold", font.style="normal", color="#ed8b00"),
                            offx=P$col1, offy=P$factoid_y, width=P$full_w, height=P$factoid_h, par.properties=parProperties(padding=0))
    }
    
    ## footnote
    if (!is.na(fn_text)) {
        if (!is.na(get_footnote_nudge(df))) {P$footnote_y <<- P$footnote_y + get_footnote_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c"),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }
}


do_BL_TL_TR <- function (df) {
    start_slide(df)
    do_title(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    preamble_text <- get_preamble(df)
    fn_text <- get_footnote(df)
    factoid_text <- get_factoid(df)
    ## setup page parameters
    page_setup(guess_preamble_lines(preamble_text), guess_footnote_lines(fn_text), guess_factoid_lines(factoid_text))
    ## add preamble if present
    do_preamble(df, preamble_text)
    
    ## if TL or TR titlebar_y override is present, then set P$T_titlebar_y accordingly
    T_titlebar_y_override(df, c("TL", "TR"))
    B_titlebar_y_override(df, c("BL", "BR"))

    do_block_layout(df, content.type="TL", has.lower=TRUE)
    do_block_layout(df, content.type="BL")
    do_block_layout(df, content.type="TR")

    ## factoid
    if (!is.na(factoid_text)) {
        P$factoid_y <<- P$footnote_y - P$factoid_ydiff;
        if (!is.na(get_factoid_nudge(df))) {P$factoid_y <<- P$factoid_y + get_factoid_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(factoid_text), font.size=14, font.family="Georgia", font.weight="bold", font.style="normal", color="#ed8b00"),
                            offx=P$col1, offy=P$factoid_y, width=P$full_w, height=P$factoid_h, par.properties=parProperties(padding=0))
    }
    
    ## footnote
    if (!is.na(fn_text)) {
        if (!is.na(get_footnote_nudge(df))) {P$footnote_y <<- P$footnote_y + get_footnote_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c"),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }
}



do_BL_BR_TL_TR <- function (df) {
    start_slide(df)
    do_title(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    preamble_text <- get_preamble(df)
    fn_text <- get_footnote(df)
    factoid_text <- get_factoid(df)
    ## setup page parameters
    page_setup(guess_preamble_lines(preamble_text), guess_footnote_lines(fn_text), guess_factoid_lines(factoid_text))
    ## add preamble if present
    do_preamble(df, preamble_text)
    
    ## if TL or TR titlebar_y override is present, then set P$T_titlebar_y accordingly
    T_titlebar_y_override(df, c("TL", "TR"))
    B_titlebar_y_override(df, c("BL", "BR"))

    do_block_layout(df, content.type="TL", has.lower=TRUE)
    do_block_layout(df, content.type="BL")
    do_block_layout(df, content.type="TR", has.lower=TRUE)
    do_block_layout(df, content.type="BR")

    ## factoid
    if (!is.na(factoid_text)) {
        P$factoid_y <<- P$footnote_y - P$factoid_ydiff;
        if (!is.na(get_factoid_nudge(df))) {P$factoid_y <<- P$factoid_y + get_factoid_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(factoid_text), font.size=14, font.family="Georgia", font.weight="bold", font.style="normal", color="#ed8b00"),
                            offx=P$col1, offy=P$factoid_y, width=P$full_w, height=P$factoid_h, par.properties=parProperties(padding=0))
    }
    
    ## footnote
    if (!is.na(fn_text)) {
        if (!is.na(get_footnote_nudge(df))) {P$footnote_y <<- P$footnote_y + get_footnote_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c"),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }
}

do_BR_TL_TR <- function (df) {
    start_slide(df)
    do_title(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    preamble_text <- get_preamble(df)
    fn_text <- get_footnote(df)
    factoid_text <- get_factoid(df)
    ## setup page parameters
    page_setup(guess_preamble_lines(preamble_text), guess_footnote_lines(fn_text), guess_factoid_lines(factoid_text))
    ## add preamble if present
    do_preamble(df, preamble_text)
    
    ## if TL or TR titlebar_y override is present, then set P$T_titlebar_y accordingly
    T_titlebar_y_override(df, c("TL", "TR"))
    B_titlebar_y_override(df, c("BL", "BR"))

    do_block_layout(df, content.type="TL", has.lower=TRUE)
    do_block_layout(df, content.type="TR", has.lower=TRUE)
    do_block_layout(df, content.type="BR")

    ## factoid
    if (!is.na(factoid_text)) {
        P$factoid_y <<- P$footnote_y - P$factoid_ydiff;
        if (!is.na(get_factoid_nudge(df))) {P$factoid_y <<- P$factoid_y + get_factoid_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(factoid_text), font.size=14, font.family="Georgia", font.weight="bold", font.style="normal", color="#ed8b00"),
                            offx=P$col1, offy=P$factoid_y, width=P$full_w, height=P$factoid_h, par.properties=parProperties(padding=0))
    }
    
    ## footnote
    if (!is.na(fn_text)) {
        if (!is.na(get_footnote_nudge(df))) {P$footnote_y <<- P$footnote_y + get_footnote_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c"),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }
}


do_B_T <- function (df) {
    start_slide(df)
    do_title(df)
    ## and page number
    DOC <<- addPageNumber(DOC)
    preamble_text <- get_preamble(df)
    fn_text <- get_footnote(df)
    factoid_text <- get_factoid(df)
    ## setup page parameters
    page_setup(guess_preamble_lines(preamble_text), guess_footnote_lines(fn_text), guess_factoid_lines(factoid_text))
    ## add preamble if present
    do_preamble(df, preamble_text)
    
    
    ## if T titlebar_y override is present, then set P$T_titlebar_y accordingly
    T_titlebar_y_override(df, c("T"))
    B_titlebar_y_override(df, c("B"))

    do_block_layout(df, content.type="T", has.lower=TRUE)
    do_block_layout(df, content.type="B")

    ## factoid
    if (!is.na(factoid_text)) {
        P$factoid_y <<- P$footnote_y - P$factoid_ydiff;
        if (!is.na(get_factoid_nudge(df))) {P$factoid_y <<- P$factoid_y + get_factoid_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(factoid_text), font.size=14, font.family="Georgia", font.weight="bold", font.style="normal", color="#ed8b00"),
                            offx=P$col1, offy=P$factoid_y, width=P$full_w, height=P$factoid_h, par.properties=parProperties(padding=0))
    }
    
    ## footnote
    if (!is.na(fn_text)) {
        if (!is.na(get_footnote_nudge(df))) {P$footnote_y <<- P$footnote_y + get_footnote_nudge(df)}
        DOC <<- addParagraph(DOC, markdown.pot(preprocess(fn_text), font.size=10, font.family="Georgia", font.weight="normal", font.style="italic", color="#87888c"),
                            offx=P$col1, offy=P$footnote_y, width=P$full_w, height=max(P$footnote_min_h, (P$footnote_bottom - P$footnote_y)), par.properties=parProperties(padding=0))
    }
}






### nasty globals for the moment...

spec <- NULL
new_slide_idx <- NULL
df <- NULL
DOC <- NULL


do_dts <- function (specfile="dts2019-rwi.xlsx") {
# do_dts <- function (specfile="dts2019-bugfix.xlsx") {
    cat("Reading spreadsheet...")
    ## load our spec
    spec <<- readWorksheetFromFile(specfile, sheet=1, header=FALSE)
    names(spec) <<- c("colA", "colB", "name", "type", "value")
    ## convenient list of all the new_slide markers so we know where to read
    new_slide_idx <<- as.integer(rownames(spec[ !is.na(spec$colA) & spec$colA == "new_slide", ]))
    read_next <- spec_reader()
    cat("Reading document template...")
    DOC <<- pptx(template=template)
    cat("\n")

    ## now start our loop to read pages from the spec
    repeat {
        cat("Reading chunk.")
        df <<- read_next()
        if (is.null(df)) {
            cat("No more chunks to read... We're done!\n")
            break
        }
        process_globals(df)
        if (!has_new_slide(df)) {
            cat("This chunk has no new slide; moving on...\n")
            next
        }
        comment <- get_comment(df)
        if (!is.na(comment)) {
            cat("Working on slide block with comment \"", comment, "\"\n", sep="")
        }
        sig <- get_content_signature(df)
        cat("Content signature:", sig, "\n")
        if (sig == "blank") {
            do_divider(df)
        } else if (sig == "TL TR") {
            do_TL_TR(df)
        } else if (sig == "T") {
            do_T(df)
        } else if (sig == "BL TL TR") {
            do_BL_TL_TR(df)
        } else if (sig == "BL BR TL TR") {
            do_BL_BR_TL_TR(df)
        } else if (sig == "BR TL TR") {
            do_BR_TL_TR(df)
        } else if (sig == "B T") {
            do_B_T(df)
        } else {
            stop("We don't handle signature ", sig, " yet")
        }
        
    } # end repeat

    # writeDoc(DOC, "reports/rwi2019-draft.pptx")
    writeDoc(DOC, "reports/rwi2019-study.pptx")
    
}
