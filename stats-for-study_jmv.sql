
-- This study analyzes 642 private-target acquisitions ($121 billion) that closed from 2016 through 2018
-- in which SRS Acquiom provided professional and financial services.


-- number of deals: 588 -> 642
select count(*) from (
   select distinct dt.deal_id
   from data.dealterms dt
   join data.buyer_rwi r on r.deal_id = dt.deal_id
   join data.line_of_business ln on ln.deal_id = dt.deal_id
      and ln.shareholder_rep='Yes'
      and extract('year' from dt.ic_deal_close_date) between 2016 and 2018 -- 2015 and 2017
) s ;

-- dollar value of those deals: $108B -> $121B
select count(*) AS deal_count,
       round((sum(deal_size) / 1E9)::numeric, 1) AS dollar_value
from (
   select distinct dt.deal_id, deal_size
   from data.dealterms dt
   join data.buyer_rwi r on r.deal_id = dt.deal_id
   join data.line_of_business ln on ln.deal_id = dt.deal_id
      and ln.shareholder_rep='Yes'
      and extract('year' from dt.ic_deal_close_date) between 2016 and 2018
) s ;

 deal_count | dollar_value 
------------+--------------
        642 |        121.6


-- explore which deals have rwi and no escrow:
select count(dt.deal_id) -- deal_name
from data.dealterms dt
join data.buyer_rwi b on dt.deal_id = b.deal_id
where b.buyer_rwi = 'Y'
  and escrow_size is null ;

select count(dt.deal_id) -- deal_name
from data.dealterms dt
join data.buyer_rwi b on dt.deal_id = b.deal_id
join data.line_of_business ln on ln.deal_id = dt.deal_id
where b.buyer_rwi = 'Y' and ln.shareholder_rep = 'Yes'
  and escrow_size is null ;


-- Number of RWI deals per year + percentages

WITH deal_counts AS (
    SELECT min(extract('year' FROM ic_deal_close_date)) AS yr, count(dt.deal_id) AS rwi_count,
	   (SELECT count(deal_id)
	    FROM data.dealterms
	    WHERE extract('year' FROM ic_deal_close_date) = min(extract('year' FROM dt.ic_deal_close_date))
	    ) AS year_count
    FROM data.dealterms dt
    JOIN data.buyer_rwi b ON (dt.deal_id = b.deal_id AND b.buyer_rwi = 'Y')
    WHERE extract('year' FROM ic_deal_close_date) BETWEEN 2015 and 2018
    GROUP BY extract('year' FROM ic_deal_close_date)
    ORDER BY extract('year' FROM ic_deal_close_date)
)
SELECT *, round(100 * rwi_count::numeric / year_count, 2) AS pct
FROM deal_counts ;

  yr  | rwi_count | year_count |  pct  
------+-----------+------------+-------
 2015 |         9 |        215 |  4.19
 2016 |        17 |        248 |  6.85
 2017 |        57 |        249 | 22.89
 2018 |        70 |        263 | 26.62


-- dollar value of those deals: $108B -> $121B

WITH deal_info AS (
   SELECT DISTINCT dt.deal_id, deal_size
   FROM data.dealterms dt
   JOIN data.buyer_rwi r ON r.deal_id = dt.deal_id
   JOIN data.line_of_business ln ON ln.deal_id = dt.deal_id
      AND ln.shareholder_rep = 'Yes'
   WHERE extract('year' from dt.ic_deal_close_date) BETWEEN 2016 AND 2018 -- 2015 and 2017
)
SELECT count(*), round(sum(deal_size)::numeric / 1E9, 2)
FROM deal_info ;


SELECT count(dt.deal_id)
FROM data.dps dt
JOIN data.buyer_rwi b ON dt.deal_id = b.deal_id
-- WHERE b.buyer_rwi = 'N'
WHERE b.buyer_rwi = 'Y'
;

