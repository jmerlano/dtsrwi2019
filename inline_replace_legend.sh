#!/bin/sh

# change c("RWI = No / Unknown", "RWI = Yes") to RWI.LEGEND


pushd /home/gkramer/bitbucket/dtsutils/chart_types/

perl -pi -e 's/c\("RWI = No \/ Unknown", "RWI = Yes"\)/RWI.LEGEND/' */*.R

