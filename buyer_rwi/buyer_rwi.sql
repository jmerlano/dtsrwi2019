DROP TABLE IF EXISTS data.buyer_rwi;

CREATE TABLE  data.buyer_rwi (
   deal_id  integer PRIMARY KEY,
   buyer_rwi text
   );


GRANT ALL ON data.buyer_rwi TO marketstandard;

-- needs to be populated by something like
-- \copy data.buyer_rwi from '/home/gkramer/buyer_rwi.csv' CSV HEADER;

