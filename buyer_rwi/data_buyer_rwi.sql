
-- Update data.buyer_rwi from data.customer

INSERT INTO data.buyer_rwi
SELECT deal_id,
       CASE WHEN buy_side_rwi THEN 'Y' ELSE 'N' END AS buyer_rwi
FROM data.customer
WHERE deal_id NOT IN
  (SELECT deal_id FROM data.buyer_rwi) ;


-- Update data.line_of_business from data.customer

INSERT INTO data.line_of_business
SELECT deal_id,
       CASE WHEN ARRAY['Shareholder Rep'] <@ business_lines THEN 'Yes' ELSE 'No' END AS shareholder_rep,
       CASE WHEN ARRAY['Option'] <@ business_lines THEN 'Yes' ELSE 'No' END AS srs_option,
       CASE WHEN ARRAY['Acquiom Escrow'] <@ business_lines THEN 'Yes' ELSE 'No' END AS acquiom_escrow,
       CASE WHEN ARRAY['AXA Escrow'] <@ business_lines THEN 'Yes' ELSE 'No' END AS axa_escrow,
       CASE WHEN ARRAY['Payments'] <@ business_lines THEN 'Yes' ELSE 'No' END AS payments
FROM data.customer
WHERE deal_id NOT IN
  (SELECT deal_id
   FROM data.line_of_business) ;


