DROP TABLE IF EXISTS data.line_of_business;

CREATE TABLE data.line_of_business (
   deal_id  integer PRIMARY KEY,
   shareholder_rep text,
   srs_option text,
   acquiom_escrow text,
   axa_escrow text,
   payments text
   );

-- grant permissions
GRANT ALL ON data.line_of_business TO marketstandard;

-- needs to be populated by something like
-- \copy data.line_of_business from '/home/gkramer/line_of_business.csv' CSV HEADER;

