
-- number of deals: 588
select count(*) from (select distinct dt.deal_id from data.dealterms dt left join data.buyer_rwi r on r.deal_id=dt.deal_id join data.line_of_business ln on ln.deal_id=dt.deal_id and ln.shareholder_rep='Yes' and extract('year' from dt.ic_deal_close_date) between 2015 and 2017) s;

-- dollar value of those deals: $108B
select sum(deal_size) from (select distinct dt.deal_id, deal_size from data.dealterms dt left join data.buyer_rwi r on r.deal_id=dt.deal_id join data.line_of_business ln on ln.deal_id=dt.deal_id and ln.shareholder_rep='Yes' and extract('year' from dt.ic_deal_close_date) between 2015 and 2017) s;
